const express = require('express');
const cors = require("cors");
const hbs = require("express-hbs");
const path = require("path");

require('dotenv').config();
const MainController = require("./controllers/main.controller");


const app = express();
app.set("view engine", "hbs");
app.engine("hbs",  require("./views/helpers")(hbs).express3({partialsDir: "./views"}));
app.set("views","./views");
app.use(express.static("views"));
app.use(express.json());
app.use(cors());

new MainController(app);

app.listen(process.env.PORT, () => {
    console.log(`Server is up on ${process.env.PORT}!`);
});
